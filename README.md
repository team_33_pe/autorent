# Autorent



## Authors
[Polgár Benedek (FB7SOY)](https://gitlab.com/benedekoszpolgar)  
[Németh Máté (IZ08EN)](https://https://gitlab.com/Hevi007)  
[Árok Dániel (ASNBPG)](https://gitlab.com/arokdani07)



## Tech stack

[Nest.js](https://nestjs.com/)

[Prisma ORM](https://www.prisma.io/)

[PostgreSQL](https://www.postgresql.org/)

[React.js](https://react.dev/)
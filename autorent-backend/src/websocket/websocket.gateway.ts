import { WebSocketGateway, WebSocketServer, SubscribeMessage, MessageBody } from '@nestjs/websockets';
import { Server } from 'socket.io';

@WebSocketGateway()
export class WebsocketGateway {
  @WebSocketServer()
  server: Server;

  @SubscribeMessage('newOffer')
  handleNewOffer(@MessageBody() data: string): void {
    this.server.emit('newOffer', data);
  }
}
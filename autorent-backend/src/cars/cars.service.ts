import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateCarDto } from './dto/cars.dto';

@Injectable()
export class CarsService {
  constructor(private prisma: PrismaService) {}

  async getAllCars() {
    return this.prisma.car.findMany({
      include: {
        category: true,
        sales: true,
      },
    });
  }

  async getCarsByCategory(category: string) {
    return this.prisma.car.findMany({
      where: { category: { name: category } },
      include: { category: true },
    });
  }

  async getCarAvailability(carId: number) {
    return this.prisma.rental.findMany({
      where: { carId },
    });
  }

  async create(createCarDto: CreateCarDto) {
    const { categoryId, brand, model, dailyPrice } = createCarDto;

    // Ellenőrizzük, hogy a kategória létezik-e
    const category = await this.prisma.category.findUnique({
      where: { id: Number(categoryId) },
    });

    if (!category) {
      throw new NotFoundException('Category not found');
    }

    return this.prisma.car.create({
      data: {
        categoryId: Number(categoryId), // Ensure the categoryId is an integer
        brand: brand,
        model: model,
        dailyPrice: Number(dailyPrice), // Ensure the dailyPrice is an integer
      },
    });
  }

  async bookCar(userId: number, carId: number, fromDate: Date, toDate: Date) {
    const fromDateObj = new Date(fromDate);
    const toDateObj = new Date(toDate);

    // Ellenőrizzük, hogy az autó elérhető-e a megadott időszakban
    const conflictingReservations = await this.prisma.rental.findMany({
      where: {
        carId,
        OR: [
          {
            fromDate: { lte: toDateObj },
            toDate: { gte: fromDateObj },
          },
        ],
      },
    });

    if (conflictingReservations.length > 0) {
      throw new Error('Car is not available for the selected dates');
    }

    // Hozzuk létre a foglalást
    return this.prisma.rental.create({
      data: {
        userId,
        carId,
        fromDate: fromDateObj,
        toDate: toDateObj,
      },
    });
  }

  async getUserRentals(userId: number) {
    return this.prisma.rental.findMany({
      where: { userId },
      include: { car: true },
    });
  }

  async getSales() {
    return this.prisma.sale.findMany({
      include: { car: true },
    });
  }

  async getCategories() {
    return this.prisma.category.findMany();
  }
}

export class CreateCarDto {
  categoryId: number;
  brand: string;
  model: string;
  dailyPrice: number;
}

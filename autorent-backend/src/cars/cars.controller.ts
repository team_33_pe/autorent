import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  Param,
  UseGuards,
} from '@nestjs/common';
import { CarsService } from './cars.service';
import { CreateCarDto } from './dto/cars.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('cars')
export class CarsController {
  constructor(private readonly carsService: CarsService) {}

  @Get()
  async getAllCars(@Query('category') category: string) {
    if (category) {
      return this.carsService.getCarsByCategory(category);
    }
    return this.carsService.getAllCars();
  }

  @Get(':id/availability')
  async getCarAvailability(@Param('id') id: string) {
    return this.carsService.getCarAvailability(Number(id));
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('book')
  async bookCar(
    @Body()
    bookCarDto: {
      userId: number;
      carId: number;
      fromDate: Date;
      toDate: Date;
    },
  ) {
    return this.carsService.bookCar(
      bookCarDto.userId,
      bookCarDto.carId,
      bookCarDto.fromDate,
      bookCarDto.toDate,
    );
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('rentals/:userId')
  async getUserRentals(@Param('userId') userId: string) {
    return this.carsService.getUserRentals(Number(userId));
  }

  @Get('sales')
  async getSales() {
    return this.carsService.getSales();
  }

  @Get('categories')
  async getCatergories() {
    return this.carsService.getCategories();
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('add')
  create(@Body() createCarDto: CreateCarDto) {
    return this.carsService.create(createCarDto);
  }
}

import { PrismaClient } from '@prisma/client';
import * as bcrypt from 'bcryptjs';

const prisma = new PrismaClient();

async function main() {
  // Töröljük a létező adatokat (opcionális)
  await prisma.rental.deleteMany();
  await prisma.sale.deleteMany();
  await prisma.car.deleteMany();
  await prisma.category.deleteMany();
  await prisma.user.deleteMany();

  // Kategóriák hozzáadása
  const sedanCategory = await prisma.category.create({
    data: { name: 'Sedan' },
  });
  const hatchbackCategory = await prisma.category.create({
    data: { name: 'Hatchback' },
  });
  const suvCategory = await prisma.category.create({ data: { name: 'SUV' } });
  const convertibleCategory = await prisma.category.create({
    data: { name: 'Convertible' },
  });
  const truckCategory = await prisma.category.create({
    data: { name: 'Truck' },
  });
  const vanCategory = await prisma.category.create({ data: { name: 'Van' } });

  // Egyesével hozzuk létre az autókat
  const car1 = await prisma.car.create({
    data: {
      brand: 'Toyota',
      model: 'Corolla',
      dailyPrice: 50,
      categoryId: sedanCategory.id,
    },
  });
  const car2 = await prisma.car.create({
    data: {
      brand: 'Honda',
      model: 'Civic',
      dailyPrice: 55,
      categoryId: sedanCategory.id,
    },
  });
  const car3 = await prisma.car.create({
    data: {
      brand: 'Ford',
      model: 'Focus',
      dailyPrice: 40,
      categoryId: hatchbackCategory.id,
    },
  });
  const car4 = await prisma.car.create({
    data: {
      brand: 'Chevrolet',
      model: 'Tahoe',
      dailyPrice: 75,
      categoryId: suvCategory.id,
    },
  });
  const car5 = await prisma.car.create({
    data: {
      brand: 'BMW',
      model: '3 Series',
      dailyPrice: 100,
      categoryId: convertibleCategory.id,
    },
  });
  const car6 = await prisma.car.create({
    data: {
      brand: 'Ford',
      model: 'F-150',
      dailyPrice: 60,
      categoryId: truckCategory.id,
    },
  });
  const car7 = await prisma.car.create({
    data: {
      brand: 'Mercedes',
      model: 'Sprinter',
      dailyPrice: 90,
      categoryId: vanCategory.id,
    },
  });

  // Egyesével hozzuk létre a felhasználókat
  const user1 = await prisma.user.create({
    data: {
      username: 'johndoe',
      name: 'John Doe',
      password: bcrypt.hashSync('password123', 10),
    },
  });
  const user2 = await prisma.user.create({
    data: {
      username: 'janedoe',
      name: 'Jane Doe',
      password: bcrypt.hashSync('password123', 10),
    },
  });
  const user3 = await prisma.user.create({
    data: {
      username: 'alice',
      name: 'Alice Smith',
      password: bcrypt.hashSync('password123', 10),
    },
  });
  const user4 = await prisma.user.create({
    data: {
      username: 'bob',
      name: 'Bob Johnson',
      password: bcrypt.hashSync('password123', 10),
    },
  });

  // Bérlések hozzáadása
  await prisma.rental.createMany({
    data: [
      {
        userId: user1.id,
        carId: car1.id,
        fromDate: new Date('2024-06-01'),
        toDate: new Date('2024-06-05'),
        created: new Date(),
      },
      {
        userId: user2.id,
        carId: car2.id,
        fromDate: new Date('2024-06-10'),
        toDate: new Date('2024-06-15'),
        created: new Date(),
      },
      {
        userId: user3.id,
        carId: car3.id,
        fromDate: new Date('2024-06-20'),
        toDate: new Date('2024-06-25'),
        created: new Date(),
      },
      {
        userId: user4.id,
        carId: car4.id,
        fromDate: new Date('2024-07-01'),
        toDate: new Date('2024-07-07'),
        created: new Date(),
      },
    ],
  });

  // Akciós ajánlatok hozzáadása
  await prisma.sale.createMany({
    data: [
      {
        carId: car1.id,
        description: '10% off for June rentals',
        percent: 10,
      },
      {
        carId: car3.id,
        description: '15% off for weekend rentals',
        percent: 15,
      },
      {
        carId: car5.id,
        description: '20% off for summer rentals',
        percent: 20,
      },
      {
        carId: car6.id,
        description: '5% off for new customers',
        percent: 5,
      },
    ],
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });

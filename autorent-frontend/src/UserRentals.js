import React, { useEffect, useState } from 'react';
import api, { setAuthToken } from './api';

const UserRentals = ({ userId, token }) => {
  const [rentals, setRentals] = useState([]);

  useEffect(() => {
    const fetchRentals = async () => {
      try {
        setAuthToken(token);
        const response = await api.get(`/cars/rentals/${userId}`);
        setRentals(response.data);
      } catch (error) {
        console.error('Error fetching rentals:', error);
      }
    };

    fetchRentals();
  }, [userId, token]);

  return (
    <div>
      <h1>Your Rentals</h1>
      <ul>
        {rentals.map((rental, index) => (
          <li key={index}>
            {rental.car.brand} {rental.car.model} - From: {new Date(rental.fromDate).toLocaleDateString()} To: {new Date(rental.toDate).toLocaleDateString()}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default UserRentals;

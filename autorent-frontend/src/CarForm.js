import React, { useState } from 'react';

const CarForm = ({ token }) => {
  const [form, setForm] = useState({
    categoryId: '',
    brand: '',
    model: '',
    dailyPrice: ''
  });

  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const formattedForm = {
      ...form,
      categoryId: parseInt(form.categoryId, 10),
      dailyPrice: parseInt(form.dailyPrice, 10),
    };
    const response = await fetch('http://localhost:3000/cars/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`  // JWT token hozzáadása a fejlécben
      },
      body: JSON.stringify(formattedForm)
    });
    const data = await response.json();
    console.log(data);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Category ID:
        <input type="number" name="categoryId" value={form.categoryId} onChange={handleChange} />
      </label>
      <label>
        Brand:
        <input type="text" name="brand" value={form.brand} onChange={handleChange} />
      </label>
      <label>
        Model:
        <input type="text" name="model" value={form.model} onChange={handleChange} />
      </label>
      <label>
        Daily Price:
        <input type="number" name="dailyPrice" value={form.dailyPrice} onChange={handleChange} />
      </label>
      <button type="submit">Add Car</button>
    </form>
  );
};

export default CarForm;

import React, { useState, useEffect } from 'react';
import Login from './Login';
import Register from './Register';
import CarList from './CarList';
import CarDetails from './CarDetails';
import UserRentals from './UserRentals';
import Sales from './Sales';
import api from './api';
import CarForm from './CarForm';

function App() {
  const [token, setToken] = useState(null);
  const [userId, setUserId] = useState(null); // Hozzáadjuk a felhasználó azonosítójának tárolását
  const [selectedCar, setSelectedCar] = useState(null);
  const [category, setCategory] = useState('');
  const [cars, setCars] = useState([]);

  useEffect(() => {
    const fetchCars = async () => {
      try {
        const response = await api.get('/cars', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setCars(response.data);
      } catch (error) {
        console.error('Error fetching cars:', error);
      }
    };

    if (token) {
      fetchCars();
    }
  }, [token, category]);

  const handleLogin = (token, userId) => {
    setToken(token);
    setUserId(userId);
  };

  const handleCategoryChange = (e) => {
    setCategory(e.target.value);
  };

  if (!token) {
    return (
      <div>
        <h1>Login</h1>
        <Login setToken={handleLogin} />
        <h1>Register</h1>
        <Register />
      </div>
    );
  }

  return (
    <div className="App">
      <h1>Car Rental System</h1>
      <Sales token={token}/>
      <CarList onSelectCar={setSelectedCar} token={token}/>
      {selectedCar && <CarDetails car={selectedCar} userId={userId} token={token}/>}
      <UserRentals userId={userId} token={token}/>
      <CarForm token={token}/>
    </div>
  );
}

export default App;

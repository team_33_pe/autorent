import React, { useEffect, useState } from 'react';
import api, { setAuthToken } from './api';

const CarDetails = ({ car, userId, token }) => {
  const [availability, setAvailability] = useState([]);
  const [fromDate, setFromDate] = useState('');
  const [toDate, setToDate] = useState('');
  const [message, setMessage] = useState('');

  useEffect(() => {
    const fetchAvailability = async () => {
      try {
        setAuthToken(token);
        const response = await api.get(`/cars/${car.id}/availability`);
        setAvailability(response.data);
      } catch (error) {
        console.error('Error fetching availability:', error);
      }
    };

    fetchAvailability();
  }, [car, token]);

  const handleBook = async () => {
    try {
      setAuthToken(token);
      await api.post('/cars/book', {
        userId,
        carId: car.id,
        fromDate,
        toDate,
      });
      setMessage('Booking successful!');
    } catch (error) {
      setMessage('Internal server error');
    }
  };

  return (
    <div>
      <h2>{car.brand} {car.model}</h2>
      <p>Category: {car.category.name}</p>
      <p>Daily Price: ${car.dailyPrice}</p>
      <h3>Availability</h3>
      <ul>
        {availability.map((rental, index) => (
          <li key={index}>
            From: {new Date(rental.fromDate).toLocaleDateString()} To: {new Date(rental.toDate).toLocaleDateString()}
          </li>
        ))}
      </ul>
      <h3>Book this car</h3>
      <input type="date" value={fromDate} onChange={(e) => setFromDate(e.target.value)} />
      <input type="date" value={toDate} onChange={(e) => setToDate(e.target.value)} />
      <button onClick={handleBook}>Book</button>
      <p>{message}</p>
    </div>
  );
};

export default CarDetails;

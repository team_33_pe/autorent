import React, { useEffect, useState } from 'react';
import api, { setAuthToken } from './api';

const CarList = ({ token, onSelectCar }) => {
  const [cars, setCars] = useState([]);
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState('');

  useEffect(() => {
    const fetchCars = async () => {
      try {
        setAuthToken(token);
        const response = await api.get('/cars');
        setCars(response.data);
      } catch (error) {
        console.error('Error fetching cars:', error);
      }
    };

    const fetchCategories = async () => {
      try {
        setAuthToken(token);
        const response = await api.get('/cars/categories');
        setCategories(response.data);
      } catch (error) {
        console.error('Error fetching categories:', error);
      }
    };

    fetchCars();
    fetchCategories();
  }, [token]);

  const filteredCars = selectedCategory
    ? cars.filter(car => car.category.name === selectedCategory)
    : cars;

  return (
    <div>
      <h1>Available Cars</h1>
      <div>
        <label htmlFor="category-select">Filter by Category:</label>
        <select
          id="category-select"
          value={selectedCategory}
          onChange={(e) => setSelectedCategory(e.target.value)}
        >
          <option value="">All</option>
          {categories.map(category => (
            <option key={category.id} value={category.name}>
              {category.name}
            </option>
          ))}
        </select>
      </div>
      <ul>
        {filteredCars.map(car => (
          <li key={car.id} onClick={() => onSelectCar(car)}>
            {car.brand} {car.model} ({car.category.name})
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CarList;

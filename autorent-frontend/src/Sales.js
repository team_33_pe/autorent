import React, { useEffect, useState } from 'react';
import api from './api';

const Sales = () => {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    const fetchSales = async () => {
      const response = await api.get('/cars/sales');
      setSales(response.data);
    };

    fetchSales();
  }, []);

  return (
    <div>
      <h1>Special Offers</h1>
      <ul>
        {sales.map((sale, index) => (
          <li key={index}>
            {sale.car.brand} {sale.car.model} - {sale.description} ({sale.percent}% off)
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Sales;
